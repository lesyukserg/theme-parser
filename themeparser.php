<?
    error_reporting(E_ERROR);
    header("Content-Type: text/html; charset=utf-8");
    define('MAINFOLDER', 'DOWNLOADED/');
    set_time_limit(300);

    $_SESSION['cnt']   = 0;
    $_SESSION['css']   = array();
    $_SESSION['files'] = array();
    
    if (!isset($_POST['src'])) $_POST['src'] = null;
    if (!isset($_POST['abs'])) $_POST['abs'] = null;
    if (!isset($_POST['css'])) $_POST['css'] = null;
    

    function parse_theme($src, $name = 'index.html')
    {
        if (strstr($name, '#')) {
            $src = substr($src, 0, strripos($src, '#'));
        }

        if (!in_array($name, $_SESSION['files'])) {

            if ($file = file_get_contents($src)) {
                create_folders(MAINFOLDER.$name);
                
                $fp = fopen(MAINFOLDER.$name, 'w');
                fwrite($fp, $file);
                fclose($fp);

                echo ++$_SESSION['cnt'] . " <font color='green'><b>файл {$name} скопирован</b></font><br>";

                $src = substr($src, 0, strripos($src, '/')) . '/';

                if (!in_array($src, $_SESSION['files'])) {
                    $_SESSION['files'][] = $name;
                    download_files($file, $src, MAINFOLDER.$name);
                }
            }
        }
    }

    function download_files($f, $src, $html)
    {
        preg_match_all("/(src|href)=(\"|')([^(\"|')]+)/i", $f, $sources);

        foreach ($sources['3'] as $file) {
            
            if (strstr($file, $src)) {
               $file_old = $file;
               $file = str_replace($src, '', $file);
               
               change_html_link($html, $file_old, $file);
            }
            
            if (!strstr($file, '.html') && !strstr($file, '.php')) {

                if (strstr($file, '.') && /* !strstr($file, '@') && */ (!strstr($file, '//') || $_POST['abs']) && !strstr($file, '#')) {
                    $to = $file;
                    
                    if (strstr($file, '//')) {
                        
                        if ($file[0] == '/') {
                            $file = 'http:'.$file;
                        }
                        
                        $from = $file;
                        $to   = 'absolute/'.substr($to, strripos($to, '/')+1, strlen($to));
                        
                        change_html_link($html, $from, $to);
                    } else {
                        
                        if ($file[0] == '/') {
                            preg_match("#(http:\/\/[^\/]+)#", $src, $site);
                            $from = $site[1] . $file;
                        } else{
                            $from = $src . $file;
                        }
                    }
                    
                    if (strstr($to,'.css')) {
                        $_SESSION['css'][] = $to;
                    }
                   
                    $to = str_replace(array('?',':',',','&'),'', $to);
                    create_folders(MAINFOLDER.$to);

                    //copy_file($from, $to);
                    advanced_copy($from, $to);
                } else {
                    echo ++$_SESSION['cnt'] . " <font color='grey'>файл {$from} -> {$to} пропущен</font><br>";
                }
            } else {
            
                if ($file[0] == '/') {
                    preg_match("#(http:\/\/[^\/]+)#", $src, $site);
                    $from = $site[1] . $file;
                } else {
                    $from = $src . $file;
                }
                
                $file = str_replace(array('?',':',',','&'),'', $file);
                parse_theme($from, $file);
            }
        }
    }

    function create_folders($f)
    {
        $s = getcwd();
        $folders = explode('/', $f);

        for ($i = 0; $i < count($folders) - 1; $i++) {
            $s .= '/' . $folders[$i];

            if (!is_dir($s)) {
                mkdir($s);
            }
        }
    }
    
    function change_html_link($html, $from, $to)
    {
        if (is_file($html)) {
            $to   = str_replace(array('?',':',',','&'),'', $to);
            $file = file_get_contents($html);
            
            if ($file) {
                $file = str_replace($from, $to, $file);
                
                echo "<font color='green'>Ссылка изменена {$from} -> {$to}</font><br>";
                
                $fp = fopen($html, 'w');
                fwrite($fp, $file);
                fclose($fp);
            }
        } else {
            echo "<font color='red'>Файл {$html} не удалось открыть</font><br>";
        }
    }

    function copy_file($from, $to)
    {
        if (!is_file(MAINFOLDER.$to) && !in_array($to, $_SESSION['files']) && $to != '/' && $to != 'absolute/') {
            
            if (copy($from, MAINFOLDER.$to)) {
                $_SESSION['files'][] = $to;
                echo ++$_SESSION['cnt'] . " <font color='green'>файл {$from} -> {$to} скопирован</font><br>";
                flush();
            } else {
                echo ++$_SESSION['cnt'] . " <font color='red'>файл {$from} -> {$to} НЕ скопирован</font><br>";
            }
        } else {
            echo ++$_SESSION['cnt'] . " <font color='grey'>файл {$from} -> {$to} уже есть</font><br>";
        }
    }
    
    function parse_css($src, $cssFiles)
    {
        if (count($cssFiles)) {
            
            foreach ($cssFiles as $css) {
                echo ++$_SESSION['cnt'] . " <font color='green'><b>Парсим {$css}</b></font><br>";
                $cssFile = MAINFOLDER.$css;
                $c = file_get_contents($cssFile);
                
                preg_match_all("/url *\(([^)]+)\)/i", $c, $sources);
                
                foreach ($sources[1] as $file) {
                    if (!strstr($file, '//') || $_POST['abs']) {
                        $file  = str_replace(array("'",'"'), '', $file);
                        $src   = substr($src, 0, strripos($src, '/')+1);
                        $css   = substr($css, 0, strripos($css, '/')+1);
                        $from  = $src.$css.$file;
                        $to    = $css.$file;
                        $toA   = str_replace(array('?',':',',','&'), '', $to);
                        
                        if ($toA != $to) {
                            if (strstr($file, '//')) {
                                change_html_link($cssFile, $file, $toA);
                            } else {
                                $file2 = str_replace(array('?',':',',','&'), '', $file);
                                change_html_link($cssFile, $file, $file2);
                            }
                        }
                        
                        create_folders(MAINFOLDER.$toA);
                        
                        advanced_copy($from, $toA);
                    } else {
                        echo ++$_SESSION['cnt'] . " <font color='grey'>файл {$from} пропущен</font><br>";
                        $_SESSION['files'][] = $to;
                    }
                }
            }
        }
    }
    
    function advanced_copy($from, $to)
    {
        $headers = array('Referer: http://enableds.com/products/drag/styles/style.css');
        
        if (!is_file(MAINFOLDER.$to) && !in_array($to, $_SESSION['files']) && $to != 'absolute/') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $from);
            curl_setopt($ch, CURLOPT_HEADER, 0); // return headers 0 no 1 yes
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return page 1:yes
            curl_setopt($ch, CURLOPT_TIMEOUT, 200); // http request timeout 20 seconds
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Follow redirects, need this if the url changes
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2); //if http server gives redirection responce
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
            curl_setopt($ch, CURLOPT_COOKIEJAR, "cookies.txt"); // cookies storage / here the changes have been made
            curl_setopt($ch, CURLOPT_COOKIEFILE, "cookies.txt");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // false for https
            curl_setopt($ch, CURLOPT_ENCODING, "gzip"); // the page encoding
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            //curl_setopt($ch, CURLOPT_PROXY, $proxy);
            //curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 3);
            $output = curl_exec($ch);
            //curl_setopt($ch, CURLOPT_PROXY, $proxy);
            //curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 3);
            $output = curl_exec($ch);
            curl_close($ch);
            
            $abs_to = getcwd().'/'.MAINFOLDER.$to;
            
            if ($output) {
                $fp = fopen($abs_to, 'w');
                fwrite($fp, $output);
                fclose($fp);
                
                echo ++$_SESSION['cnt'] . " <font color='green'>файл {$from} -> {$to} скопирован</font><br>";
                $_SESSION['files'][] = $to;
                
            } else {
                echo ++$_SESSION['cnt'] . " <font color='red'>файл {$from} -> {$to} НЕ скопирован</font><br>";
            }
        } else {
            echo ++$_SESSION['cnt'] . " <font color='grey'>файл {$from} -> {$to} уже есть</font><br>";
        }
        flush();
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Google image parser</title>
        <style>
            input[type="submit"] {
                width: 300px;
            }

            body {
                font: 13px/18px Verdana, Arial, Tahoma, sans-serif;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                color: #FFF;
                background: #39404C;
                overflow-y: scroll
            }

            .section {
                clear: both;
                position: relative;
                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
                margin: 0 0 20px
            }

            .section__headline {
                background: #E05C50;
                background: linear-gradient(#EF705F, #E05C50) repeat scroll 0 0 rgba(0, 0, 0, 0);
                box-shadow: 0 1px #F08C75 inset;
                font-size: 16px;
                padding: 8px 14px;
                text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2)
            }

            .section__inner {
                color: #444444;
                padding: 15px;
                background: #EFEFEF
            }
            
            .row.archivelog {
                height: 580px;
                overflow: auto;
            }

            input[type="submit"] {
                height: 34px;
                cursor: pointer;
                font: 16px Verdana, Arial, sans-serif;
                border: none;
                color: #FFF;
                box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
                text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2);
                padding: 0 15px;
                background: #94CF58;
                background: linear-gradient(#94CF58, #85CA40);
                margin: 10px auto
            }

            input[type="submit"]:hover {
                background: linear-gradient(#A4DF68, #95DA50)
            }

            input[type="submit"]:active {
                box-shadow: 0 0 0 !important;
                background: #85CA40
            }

            input[type="submit"][disabled="disabled"] {
                background: none repeat scroll 0 0 #888888;
                box-shadow: 0 0 0
            }
        </style>
    </head>
    <body>
        <h1>Парсер тем с сайта themeforest.net</h1>

        <form method="POST">
            <section class="section choose_dir">
                <div class="section__headline">Введите адресс темы:</div>
                <div class="section__inner">
                    <div class="row">
                        <input type="text" name="src" value="<?= $_POST['src'] ?>" style="width:99%" placeholder="http://preview.ab-themes.com/shard-html/"/> http://preview.ab-themes.com/shard-html/<br/>
                       <input type="checkbox" name="abs" value="1" <?= $_POST['abs'] ? 'checked="checked"' : '' ?> /> Скачивать ресурсы в абсолютных ссылках<br/>
                    </div>
                </div>
            </section>
            <input id="submit_SERG" type="submit" name="PARSE" value="СТАРТ" <?= $_POST['src'] ? 'disabled="disabled"' : '' ?> onclick="this.setAttribute('disabled', 'disabled')"/>
        </form>

        <section class="section">
            <div class="section__headline">Процесс парсинга</div>
            <div class="section__inner">
                <div class="row archivelog">
                    <div>
                        <?
                        if ($_POST['src']) {
                            echo '<font color="blue">>>>ПАРСИНГ ТЕМЫ</font><br>';
                            parse_theme($_POST['src']);
                            echo '<font color="blue"><<<ПАРСИНГ ЗАВЕРШЕН</font><br>';
                            //$_SESSION['css'] = array('styles/font-awesome.css');
                            echo '<font color="blue">>>>ПАРСИНГ файлов из CSS</font><br>';
                            parse_css($_POST['src'], $_SESSION['css']);
                            echo '<font color="blue"><<<ПАРСИНГ из CSS ЗАВЕРШЕН</font><br>';
                            echo '<script>document.getElementById("submit_SERG").removeAttribute("disabled")</script>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>